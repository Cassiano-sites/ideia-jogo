# Ideia de jogo:

## 1 - Ideia Principal
1. um hackslash "top down (quase)"
    - like The Slormancer...
1. dungeon crawler
    - like The Binding of Isaac...
1. town managing
    - like Stardew Valley...
1. business simulation game
    - like Moonlighter...
1. jogador é um samurai
## 2 - Lógicas Principais
1. armas evoluem em conjunto ao jogador
    - as armas coletam as almas dos monstros e evoluem
    - as armas possuem uma quantidade máxima de almas que podem ser coletadas
    - quando chega ao máximo a arma possui o jogador
1. é possível cuidar de uma fazenda
1. é possível vender ítens em uma lojinha
## 3 - Mapa
1. mapa é gerado automaticamente... <https://www.youtube.com/watch?v=7BOKVraXhk8>
    - é usado um grid de 11 x 11 (room inicial  > room1 > room2 > room3 > room 4 > roomB)
    - é calculado aleatoriamente entre os números dentro de max e min
        * max de rooms é de 5
        * min é de 3
    - rooms retangulares
    - cada room só pode criar um room bônus (chance de 0.25)
        * max 2 rooms bônus por andar
        * room bônus não cria outros rooms
    - é escolhido um lado aleatório (entre CIMA - DIREITA - ESQUERDA - BAIXO)
        * testa se possui room na posição
            + se tiver (room bônus incluso) escolhe otra posição
            + loop até criar novo room
        * porta para o lado escolhido
        * room bônus é desconsiderado
    - salas são feitas à mão
        * portas colocadas após lógica de criação do mapa
1. cidades que jogador poderá passar

    país do jogador     | país inimigo do jogador
    ------------------- | -------------------
    --                  | Nagabushi (cidade inicial) - Samurai longo
    Shirokachi (capital) - Valor branco | Kurokachi (capital) - Valor preto
    --                  | --
## 4 - Cidade
1. fazenda
    - é possível cuidar de plantas e vegetais
1. loja
    - serve para vender itens coletáveis para npcs
1. templo (casa de _Shuichi_ e _Yoki_)
    - serve para diminuir a quantidade de almas de alguma arma
1. forja
    - é para a ciação de armas (não equipáveis)
1. casas
    - _Mahina_, _Yudi_, _Hayato_ e _Mina_
    - _Yuri_ e _Saori_
## 5 - Itens
1. armas
1. comidas
1. coletáveis
## 6 - História
### Começo do jogo
> O jogador é um guerreiro que faz parte do exército de _**nomeDoPaís**_. 

O jogador é mandado para uma pequena vila (em um lugar estratégico na guerra) para destruí-la, porém no caminho há uma floresta cheia de demônios (o exército não sabe desse fato).

O exército do jogador é destruído... Ele foge para longe da batalha.

----------------
***TUTORIAL 1***
> O jogador poderá escolher se sim ou se não
>> Se ele escolher não, pula o tutorial.
----------------
Porém um demônio o persegue...
> O jogador luta contra o demônio e perde.

Quando o demônio está quase a matá-lo, ele consegue escapar e se esconder.
O jogador desmaia

**Depois de algum tempo**

Quando o jogador acorda ele está dentro de um pequeno quarto.
Alguém entra e fala:
```
Pessoa desconhecida: Ah... Você acordou!!!
Jogador: Onde estou?
Pessoa desconhecida: Hmm? Na minha casa... aqui...
```
A pessoa dá ao jogador um pouco de comida.
```
Pessoa desconhecida: A propósito, Meu nome é Yuri. Qual o seu?
Jogador: Meu nome é <nome>...
Yuri: <nome> então é...? Quando estiver bem vá para a casa da Mahina. Foi ela quem te trouxe aqui.
Jogador: Ok... Vou terminar aqui e vou direto para lá...
Yuri: A casa dela é a com os couros de animais na frente... é bem difícil errar.
```
Yuri sai do quarto.
> O jogador começa a controlar o personagem.

**Chegando na casa de Mahina**

```
Jogador: Hm... Alguém em casa?
--- Silencio ---
Jogador: Hmm... Talvez eles tenham saído. Vou dar uma vol...
--- BLAM ---
Garoto desconhecido: AAAAAAAIIII.... Hm??? Quem é você?
Jogador: Hmmm... Eu sou <nome>... A Mahina está aqui?
Garoro desconhecido: A Mahi? O que você quer com ela?
Jogador: Foi ela que me trouxe aqui... Eu quero agradecê-la...
Garoro desconhecido: Ah... Você é o cara que ela trouxe da floresta! Então... Ela está caçando com a nossa mãe... Com licença?
```
O garoto entra na casa correndo e faz um grande barúlho. Assim como quando sai da mesma.
O jogador dá a volta na casa, há uma parte da floresta, ele entra e vê duas mulheres escondidas de um cervo.
A garota atira uma flecha e acerta na cabeça do cervo.
```
Mulher desconhecida: Boa garota! Assim que eu te ensinei.
Garota desconhecida: Rhm...
Mulher desconhecida: Agora temos comida para o jantar. Chame seu pai para ele cortar o cervo. Acredito que ele esteja na casa de Shuichi.
Garota desconhecida: Ok, vou chamá-lo.
```
A garota se vira e vê o jogador. Ela vai em sua direção.
```
Garota desconhecida: Você acordou então...
Jogador: Sim... Você é Mahina?
Garota desconhecida: Sim.
Jogador: Ah... Bem... Obrigado por me salvar quando eu desmaiei na floresta.
Mahina: De nada. Eu tenho que ir chamar meu pai. Com licença.
Jogador: Hm... Ok. 
```
Mahina sai da floresta e vai chamar seu pai. A mulher que estava junto de Mahina chega perto do jogador e fala:
```
Mulher desconhecida: Ela não é muito de conversar... Aliás, meu nome é Mina.
Jogador: É eu percebi, você é a irmã dela?
Mina: AHAHAHA... Não... Eu sou a mãe dela... Hihihi... Pareço tão jovem assim?
Jogador: Ah... Hm... Sim.
Mina: Entendo. Hihi... Bem, nós temos bastante janta, não quer se juntar a nós?
Jagador: Se não for muito incomodo...
```

**Pouco tempo depois**

```
Mina: Venha <nome>.
Jogador: Ok.
```
O jogador entra na casa atrás de Mina
```
Mina: Esse é meu marido Hayato, meu filho Yudi, e bem... acho que você já conhece a minha filha Mahina.
Jogador: Boa noite.
Yudi: Yo...
Mahina: Olá...
Hayato: Olá, eu sou o curteiro da cidade, se precisar que eu corte algumanimal para você é só pedir. Claro não será de graça... Hahah...
Jogador: Rhm... Eu pensarei nisso...
Mina: Venha vamos comer!
```
Todos se juntam na mesa para comer.
```
Hayato: Então... Como você chegou a desmaiar na floresta?
Jogador: É uma longa história... Mas por causa da guerra que está ocorrendo, o exército teve que entar na floresta. Porém não sabíamos que teriam tantos monstros, e que eles seriam tão fortes.
Hayato: É... Esses demônios são muito chatos mesmo. De vez enquanto eles tentam entrar na vila, mas não podemos deixar!
Mina: Quando estiver curado... Fará o que?
Jogador: Acho que irei voltar para a minha cidade natal... Não aguento mais a guerra... Não tenho muito dinheiro, mas terei que dar um jeito de viver em paz...
Hayato: Hmm... Tem algo na sua cidade natal que te trás vontade de voltar?
Jogador: Na verdade não, porém é onde eu nasci e onde minha mãe está enterrada.
```
A janta termina com um clima meio estranho...
```
Jogador: Anrham... Então... É... Enquanto eu estou por aqui, eu poderia ter algumas aulas para me proteger?
Mina: Hm? Ah, claro! Você precisará aprender a como matar esses demônios para poder voltar. Eu lhe ensinarei tudo que sei...
Hayato: Ha! Tive uma ideia. Que tal a Mahina o ensinar? ela já terminou o treinamento dela...
Mina: Ah! Ótima ideia!
Mahina: Por mim tanto faz.
Hayato: Então está decidido. Mahina lhe ensinará o que você vai prescisar para não morrer quando for para a floresta.
Jogador: O-Obrigado...
Mahina: Lhe esperarei na floresta atrás de casa amanhã depois do sol raiar, não se atrase.
Jogador: Claro!
```

**No outro dia**

O jogador acorda na casa de Yuri, ainda está escuro, ele vai para a cozinha para tomar seu café da manhã.
```
Yuri: Bom dia.
Jogador: Bom dia.
Yuri: O café esta na mesa pode comer. Vou chamar a Saori.
Jogadpr: Ok.
```
O jogador senta na mesa e prepara um pão.
```
Saori?: Bom dia, eu sou a Saori.
Jogador: Bom dia, meu nome é <nome>.
Yuri: O sol está para raiar, você não vai querer chegar atrasado no seu primeiro dia de treino com Mahina.
Jogador: Sim Sim... Estou indo então, até mais tarde. E obrigado.
``` 
> O jogador começa a controlar o personagem


**Na floresta (atrás da casa de Mahina)**

```
???: Você até que chegou cedo.
Jogador: Hm? Ah... Bom dia.
Mahina: Bom dia. Antes de começar... Você quer mesmo fazer o treinamento?
```
----------------
***TUTORIAL 2***
> O jogador poderá escolher se sim ou se não
>> Se ele escolher não, pula o tutorial.
----------------
```
Mahina: Bom... Você tem jeito para a coisa. Minha mãe pediu para eu lhe mostrar a vila depois do treinamento. Se não for muito problema para você.
Jogador: Não... Ahn... Só tenho que... Hahn... Descansar um pouco. Ahn...
```
Corta para eles estando na frente da casa de Mahina.
```
Mahina: Olhe. Aquele é o templo, o tio Shuichi é quem cuida de lá, além de ser também o orfanato da vila.
Mahina: Aquele é a forja, quem cuida de lá é a Kin.
Mahina: Tem as fazendas que são divididas em seis... Mas só cinco estão em uso... O antigo dono da sexta morreu contra os demônios e não tem ninguem que possa cuidar do espaço.
Mahina: E por fim tem a loja da vila. Quem cuida dela é o velho -- e a velha --. Eu recomendo você ir lá agora, eles podem te vender algumas roupas extra ou armas.
Jogador: Ok.
```
Mahina entra em sua casa.
> Jogador controla o personagem

**Chegando na loja**
### JOOJ
## 7 - Personagens

Nome      | Sexo   | Idade | Profição   | Parentesco
--------- | ------ |------ | ---------- | ----------
_Yuri_    | Homem  | 26    | Fazendeiro | Casado com _Saori_
_Saori_   | Mulher | 23    | Fazendeira | Irmã de _Yasu_. Casada com _Yuri_
_Mahina_  | Mulher | 21    | Caçadora   | Irmã de _Yudi_. Filha de _Hayato_ e _Mina_
_Yudi_    | Homem  | 16    | Ajudante de Curteiro | Irmão de _Mahina_. Filho de _Hayato_ e _Mina_
_Hayato_  | Homem  | 46    | Curteiro   | Irmão de _Shuichi_. Pai de _Hahina_ e _Yudi_. Casado com _Mina_
_Mina_    | Mulher | 38    | Caçadora   | Mãe de _Mahina_ e _Yudi_. Casada com _Hayato_
_Shuichi_ | Homem  | 42    | Druida     | Irmão de _Hayato_. Pai de _Yoki_
_Yoki_    | Mulher | 19    | Ajudante de Druida | Filha de _Shuichi_
_Kin_     | Mulher | 35    | Ferreira   | Mãe de _Hidetaka_. Casada com _Yasu_
_Yasu_    | Mulher | 29    | Guarda     | Irmã de _Saori_. Casada com _Kin_
_Hidetaka_| Homem  | 14    | Ajudante de Ferreiro | Filho de _Kin_
__        | Homem  | 62    | Vendedor   | Casado com __
__        | Mulher | 61    | Vendedor   | Casada com __

![foto em familia](familias_do_jogo.png)
## 8 - História dos personagens principais e missões secundárias
### _Yuri_
* História
* Missões secundárias
    1. Missão 1
### _?Mahina?_
* História
* Missões secundárias
    1. Missão 1
### _Yudi_
* História
* Missões secundárias
    1. Missão 1
### _Hayato_
* História
* Missões secundárias
    1. Missão 1
### _Mina_
* História
* Missões secundárias
    1. Missão 1
### _Shuichi_
* História
* Missões secundárias
    1. Missão 1
### _Yoki_
* História
* Missões secundárias
    1. Missão 1
### _Kin_
* História
* Missões secundárias
    1. Missão 1
### _Yasu_
* História
* Missões secundárias
    1. Missão 1
### __
* História
* Missões secundárias
    1. Missão 1
## 9 - Conversas
1. texto em Scriptable Objects
    - pode ser um array que cada opção é outro array com os itens
    - é escolhido entre:
        * text
            + possui campo de nome
            + possui campo de texto para falas
        * choose
            + abre um campo para quantidade de escolhas
            + em cada escolha tem espaço para outro Scriptable Object e o texto da escolha
            + acaba a sequência de texto
        * shop
            + campo para os itens a serem vendidos e seus preços
        * end
            + acaba a sequência de texto
    - algo como:
        ```
        Tipo de texto: [Text]
            {Nome: [""], Fala: [""]}
        Tipo de texto: [Choose]
            length: [2]
            [
                {Choose #1: [""], Next Object: [obj]},
                {Choose #2: [""], Next Object: [obj]}
            ]
        Tipo de texto: [Shop]
            length: [2]
            [
                {
                    Item #1:,
                    {Name: [""], Price: [""]}
                },
                {
                    Item #2:,
                    {Name: [""], Price: [""]}
                }
            ]
        Tipo de texto: [End]
            !!"Is not possible to continue the conversation!"!!
        ```
1. script de começo de falas fica no player
## 10 - Resumo
## OUTRAS IDEIAS
    5 - 4. ??? armaduras ??? 
    ? - ?? Classes ??
